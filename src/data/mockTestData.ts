import { FullLocationRequest } from "../types/weatherTypes";

export const testFullRequest: FullLocationRequest = {
  date: "10/20/2018",
  time: "8am",
  scale: "F",
  status: "day",
  temperature: 70,
  highTemperature: 100,
  lowTemperature: 50,
  precipitation: 90,
  humidity: 80,
  pollen: "good",
  UV: "none",
  forecast: [
    { temperature: 70, time: "8am", status: "day", precipitation: 0 },
    { temperature: 71, time: "9am", status: "day", precipitation: 0 },
    { temperature: 72, time: "10am", status: "day", precipitation: 0 },
    { temperature: 72, time: "11am", status: "day", precipitation: 0 },
    { temperature: 72, time: "12am", status: "day", precipitation: 0 },
    { temperature: 60, time: "1pm", status: "rain", precipitation: 10 },
    { temperature: 65, time: "2pm", status: "rain", precipitation: 20 },
    { temperature: 65, time: "3pm", status: "rain", precipitation: 30 },
    { temperature: 69, time: "4pm", status: "rain", precipitation: 30 },
    { temperature: 60, time: "5pm", status: "day", precipitation: 0 },
    { temperature: 59, time: "6pm", status: "day", precipitation: 0 },
    { temperature: 55, time: "7pm", status: "day", precipitation: 0 },
    { temperature: 55, time: "8pm", status: "night", precipitation: 0 },
    { temperature: 55, time: "9pm", status: "night", precipitation: 0 },
    { temperature: 55, time: "10pm", status: "night", precipitation: 0 },
    { temperature: 54, time: "11pm", status: "night", precipitation: 0 },
    { temperature: 54, time: "12am", status: "night", precipitation: 0 },
    { temperature: 53, time: "1am", status: "night", precipitation: 0 },
    { temperature: 53, time: "2am", status: "night", precipitation: 0 },
    { temperature: 53, time: "3am", status: "night", precipitation: 0 },
    { temperature: 53, time: "4am", status: "night", precipitation: 0 }
  ]
};
