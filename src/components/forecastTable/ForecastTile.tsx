import { h, FunctionalComponent } from "preact";
import { Weather, WeatherStatus } from "../../types/weatherTypes";
import WeatherIcon from "../weatherInfo/WeatherIcon";
import { numberToTemperature } from "../../data/formatter";

const styles = require("./ForecastTableStyles.css");

const ConditionsWithPrecipitation = new Set<WeatherStatus>(["rain", "snow"]);

type Props = Readonly<{
  forecast: Weather;
}>;

const ForecastTile: FunctionalComponent<Props> = props => {
  const hasPrecipitation = ConditionsWithPrecipitation.has(
    props.forecast.status
  );
  const iconSize = hasPrecipitation ? 20 : 35;
  return (
    <td className={styles["forecast-table-cell"]}>
      <div className={styles["forecast-time"]}>{props.forecast.time}</div>
      {hasPrecipitation && (
        <div className={styles["forecast-precipitation-precentage"]}>
          {props.forecast.precipitation}%
        </div>
      )}
      <WeatherIcon status={props.forecast.status} widthPixels={iconSize} />
      <div>{numberToTemperature(props.forecast.temperature)}</div>
    </td>
  );
};

export default ForecastTile;
