import { h, FunctionalComponent } from "preact";

import { Forecast } from "../../types/weatherTypes";
import ForecastTableRows from "./ForecastTableRows";

const styles = require("./ForecastTableStyles.css");

type Props = Readonly<{
  forecast: Forecast;
}>;

const ForecastTable: FunctionalComponent<Props> = props => (
  <table id={styles["forecast-table"]}>
    <caption id={styles["forecast-table-caption"]}>Forecast</caption>
    <ForecastTableRows forecast={props.forecast} width={6} />
  </table>
);

export default ForecastTable;
