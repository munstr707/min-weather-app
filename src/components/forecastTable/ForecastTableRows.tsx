import { h, FunctionalComponent } from "preact";
import { Forecast } from "../../types/weatherTypes";

import ForecastTile from "./ForecastTile";

const buildForecastTableCell = (width: number) => (
  rows: any,
  currentForecastEntry: any,
  index: number
) => {
  const forecastTile = <ForecastTile forecast={currentForecastEntry} />;
  return (
    (index % width === 0
      ? rows.push([forecastTile])
      : rows[rows.length - 1].push(forecastTile)) && rows
  );
};

type Props = Readonly<{
  forecast: Forecast;
  width: number;
}>;

const ForecastTableRows: FunctionalComponent<Props> = props => {
  const addForecastTileToRows = buildForecastTableCell(props.width);
  return (
    <div>
      {props.forecast
        .slice(1, 19) // delegated to server in future
        .reduce(addForecastTileToRows, new Array<Array<JSX.Element>>())
        .map((forecastRow: any) => (
          <tr>{forecastRow}</tr>
        ))}
    </div>
  );
};

export default ForecastTableRows;
