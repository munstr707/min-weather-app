import { h, FunctionalComponent } from "preact";

const styles = require("./appStyles.css");

import Header from "./header/header";
import WeatherInfo from "./weatherInfo/weatherInfo";

const App: FunctionalComponent = () => (
  <div id="app" className={styles.app}>
    <Header />
    <WeatherInfo />
  </div>
);

export default App;
