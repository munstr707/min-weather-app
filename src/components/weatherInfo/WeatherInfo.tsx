import { h, Component } from "preact";

import Conditions from "./Conditions";
import ForecastTable from "../forecastTable/ForecastTable";
import WeatherIcon from "./WeatherIcon";

import { FullLocationRequest } from "../../types/weatherTypes";

import { testFullRequest } from "../../data/mockTestData";

import { numberToTemperature } from "../../data/formatter";

const styles = require("./weatherInfoStyles");

type State = { weatherData: FullLocationRequest | undefined };

class WeatherInfo extends Component<{}, State> {
  state = {
    weatherData: undefined
  };

  componentDidMount() {
    this.setState({
      weatherData: testFullRequest
    });
  }

  render(_: {}, state: State) {
    return state.weatherData === undefined ? (
      <p>loading...</p>
    ) : (
      <div className={styles["parent-container"]}>
        <span className={styles["last-updated"]}>{`${state.weatherData.date} ${
          state.weatherData.time
        }`}</span>
        <span className={styles.location}>Kansas City, MO</span>

        <span className={styles["current-weather"]}>
          <span className={styles["current-status"]}>
            <WeatherIcon status="cloud" widthPixels={95} />
          </span>
          <span className={styles["high-low-temp-container"]}>
            <span
              className={styles["high-temperature"]}
            >{`${numberToTemperature(
              state.weatherData.highTemperature
            )}↑`}</span>
            <span className={styles["low-temperature"]}>{`${numberToTemperature(
              state.weatherData.lowTemperature
            )}↓`}</span>
          </span>
          <span
            className={styles["current-temperature"]}
          >{`${numberToTemperature(state.weatherData.temperature)}`}</span>
        </span>

        <span className={styles.conditions}>
          <Conditions
            humidity={state.weatherData.humidity}
            pollen={state.weatherData.pollen}
            precipitation={state.weatherData.precipitation}
            UV={state.weatherData.UV}
          />
        </span>
        <span className={styles.forecast}>
          <ForecastTable forecast={state.weatherData.forecast} />
        </span>
      </div>
    );
  }
}

export default WeatherInfo;
