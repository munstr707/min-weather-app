import { h, FunctionalComponent } from "preact";
import ConditionIcon from "./ConditionIcon";

const styles = require("./ConditionsStyles.css");

type Props = Readonly<{
  humidity: number;
  precipitation: number;
  pollen: string;
  UV: string;
}>;

const Conditions: FunctionalComponent<Props> = props => (
  <span id={styles["condition-parent"]}>
    <ConditionIcon condition="precipitation" />
    <span className={styles["condition-value"]}>{`${
      props.precipitation
    }%`}</span>
    <ConditionIcon condition="humidity" />
    <span className={styles["condition-value"]}>{`${props.humidity}%`}</span>
    <ConditionIcon condition="air quality" />
    <span className={styles["condition-value"]}>{props.pollen}</span>
    <ConditionIcon condition="UV" />
    <span className={styles["condition-value"]}>{props.UV}</span>
  </span>
);

export default Conditions;
