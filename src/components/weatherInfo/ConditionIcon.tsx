import { h, FunctionalComponent } from "preact";
import { Conditions } from "../../types/weatherTypes";

const raindropSVGPath = require("../../assets/icons/weatherIcons/raindrop.svg");
const raindropsSVGPath = require("../../assets/icons/weatherIcons/raindrops.svg");
const windSVGPath = require("../../assets/icons/weatherIcons/wind.svg");
const sunConditionSVGPath = require("../../assets/icons/weatherIcons/sun-condition.svg");

const conditionIconPath: Readonly<{ [condition in Conditions]: any }> = {
  "air quality": windSVGPath,
  humidity: raindropSVGPath,
  precipitation: raindropsSVGPath,
  UV: sunConditionSVGPath
};

type Props = Readonly<{
  condition: Conditions;
}>;

const ConditionIcon: FunctionalComponent<Props> = props => (
  <img
    src={conditionIconPath[props.condition]}
    alt={`${props.condition} icon`}
    style={{ width: "30px", marginLeft: "5px" }}
  />
);

export default ConditionIcon;
