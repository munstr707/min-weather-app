import { h, FunctionalComponent } from "preact";

const cloudImagePath = require("../../assets/icons/weatherIcons/cloud.svg");
const moonImagePath = require("../../assets/icons/weatherIcons/moon.svg");
const rainyImagePath = require("../../assets/icons/weatherIcons/rainy.svg");
const snowyImagePath = require("../../assets/icons/weatherIcons/snowy.svg");
const sunImagePath = require("../../assets/icons/weatherIcons/sun.svg");

import { WeatherStatus } from "../../types/weatherTypes";

const weatherIconPath: Readonly<{ [status in WeatherStatus]: any }> = {
  cloud: cloudImagePath,
  day: sunImagePath,
  night: moonImagePath,
  rain: rainyImagePath,
  snow: snowyImagePath
};

type Props = Readonly<{
  status: WeatherStatus;
  widthPixels?: number;
}>;

const pixelValue = (value: number) => `${value}px`;

const WeatherIcon: FunctionalComponent<Props> = props => (
  <img
    src={weatherIconPath[props.status]}
    alt={`${props.status} icon`}
    style={{
      ...(props.widthPixels && { width: pixelValue(props.widthPixels) })
    }}
  />
);

export default WeatherIcon;
