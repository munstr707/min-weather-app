import { h } from "preact";

const style = require("./headerStyles");

const Header = () => (
  <header class={style.header}>
    <h1>MW</h1>
  </header>
);

export default Header;
