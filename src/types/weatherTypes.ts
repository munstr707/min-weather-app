export type WeatherStatus = "day" | "night" | "cloud" | "rain" | "snow";
export type Conditions = "air quality" | "humidity" | "precipitation" | "UV";

// Time format: hh:mm:ss - Military time, convert to am/pm

type StatusRatings = "none" | "good" | "moderate" | "bad";
export type PollenStatus = StatusRatings;
export type UVStatus = StatusRatings;

export type TemperatureScale = "F" | "C";

export type ForecastEntry = Readonly<{
  time: string;
  condition: WeatherStatus;
  precipitation: number;
  temperature: number;
}>;

export type Forecast = ReadonlyArray<Weather>;

export type Weather = Readonly<{
  status: WeatherStatus;
  temperature: number;
  time: string;
  precipitation: number;
}>;

export type CurrentWeather = Weather &
  Readonly<{
    date: string;
    highTemperature: number;
    lowTemperature: number;
    humidity: number;
    pollen: PollenStatus;
    UV: UVStatus;
    forecast: Forecast;
  }>;

export type FullLocationRequest = CurrentWeather &
  Readonly<{ scale: TemperatureScale }>;
